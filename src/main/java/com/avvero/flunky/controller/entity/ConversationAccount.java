package com.avvero.flunky.controller.entity;

import lombok.Data;

/**
 * @author fxdev-belyaev-ay
 */
@Data
public class ConversationAccount {

    private boolean isGroup;
    private String id;
    private String name;

}

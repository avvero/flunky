package com.avvero.flunky.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.task.TaskExecutor;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.scheduling.PollerMetadata;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.scheduling.support.PeriodicTrigger;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static java.util.Arrays.asList;

/**
 * Integration конфигурация, описаны каналы для событий:
 * - смена статуса участия
 * - смена статуса тура
 *
 * @author fxdev-belyaev-ay
 */
@Configuration
@IntegrationComponentScan
public class IntegrationConfig {

    @Autowired
    TaskExecutor taskExecutor;

    @Autowired
    private Environment env;

    @Bean(name = PollerMetadata.DEFAULT_POLLER)
    public PollerMetadata defaultPoller() {
        PollerMetadata pollerMetadata = new PollerMetadata();
        pollerMetadata.setTrigger(new PeriodicTrigger(1000));
        return pollerMetadata;
    }

    /**
     * Channel для событий смены статаса пользователя
     *
     * @return
     */
    @Bean
    public MessageChannel webHookMessageChannel() {
        return new PublishSubscribeChannel(taskExecutor); //async
    }

    /**
     * Flow для событий смены статусы тура.
     * При смене статуса отправляются email на каждый адрес, перечисленный в конфиге tour_change_state.recipients
     *
     * @param webHookMessageChannel
     * @return
     */
    @Bean
    public IntegrationFlow tourChangeStateFlow(MessageChannel webHookMessageChannel) {
        return IntegrationFlows
                .from(webHookMessageChannel)
                .handle(message -> {
                    responseChannel().send(message);
                })
                .get();
    }

    /**
     * Очередь отправки сообщений
     *
     * @return
     */
    @Bean
    public QueueChannel responseChannel() {
        return new QueueChannel();
    }
}

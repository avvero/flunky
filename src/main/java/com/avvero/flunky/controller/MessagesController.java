package com.avvero.flunky.controller;

import com.avvero.flunky.controller.entity.ConversationMessage;
import com.avvero.flunky.service.EchoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageChannel;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.messaging.support.MessageBuilder.withPayload;

/**
 * @author Avvero
 */
@Slf4j
@RestController
@RequestMapping("/api/messages")
public class MessagesController {

    @Autowired
    EchoService echoService;
    @Autowired
    MessageChannel webHookMessageChannel;

    @RequestMapping(method = RequestMethod.POST)
    public void messages(@RequestBody ConversationMessage conversationMessage) {
        log.info(conversationMessage.toString());

        webHookMessageChannel.send(withPayload(conversationMessage).build());
    }

}

package com.avvero.flunky.controller;

import com.avvero.flunky.service.MsOAuthService;
import com.avvero.flunky.service.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author fxdev-belyaev-ay
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    MsOAuthService msOAuthService;

    @RequestMapping(value = "/token", method = RequestMethod.GET)
    public Token token() {
        return msOAuthService.token();
    }

}

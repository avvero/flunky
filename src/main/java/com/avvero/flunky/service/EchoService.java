package com.avvero.flunky.service;

import com.avvero.flunky.controller.entity.ConversationMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

/**
 * @author fxdev-belyaev-ay
 */
@Service
public class EchoService {

    @Autowired
    ApisService apisService;
    @Autowired
    MsOAuthService msOAuthService;

    @ServiceActivator(inputChannel = "responseChannel")
        public void echo(Message message) {
        ConversationMessage conversationMessage = (ConversationMessage) message.getPayload();
        ConversationMessage echo = new ConversationMessage();
        echo.setText("echo: " + conversationMessage.getText());
        echo.setType("message/text");
        apisService.send(msOAuthService.token().getAccess_token(), conversationMessage.getConversation().getId(), echo);
    }

}

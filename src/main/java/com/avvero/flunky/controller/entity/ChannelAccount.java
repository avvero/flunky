package com.avvero.flunky.controller.entity;

import lombok.Data;

/**
 * @author fxdev-belyaev-ay
 */
@Data
public class ChannelAccount {

    private String id;
    private String name;

}

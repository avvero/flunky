package com.avvero.flunky.service;

import com.avvero.flunky.controller.entity.ConversationMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author fxdev-belyaev-ay
 */
@Slf4j
@Service
public class ApisService {

    @Autowired
    private Environment env;

    public String send(String token, String conversationId, ConversationMessage message) {
        String url = String.format(env.getRequiredProperty("skype.apis.v3.conversations.activities.url"),
                conversationId);
        RestTemplate restTemplate = new RestTemplate();
        log.info("--Start send--");
        log.info("Request: " + message.toString());
        HttpEntity<ConversationMessage> request = new HttpEntity(message, createHeaders(token));
        ResponseEntity<String> response = restTemplate.postForEntity(url, request, String.class);
        log.info("Response: " + response.getBody());
        log.info("--Stop send--");
        return response.getBody();
    }

    HttpHeaders createHeaders(final String token) {
        return new HttpHeaders() {
            {
                set("Authorization", "Bearer " + token);
            }
        };
    }

}
